from sanic import Sanic
from sanic.response import json
from sanic import Blueprint
from util.db import db as model

# 获取app实例
app = Sanic.get_app()

# 配置信息
config = app.config

# https://github.com/kennethreitz/records
db = config['DB_CONN'].get_connection()

# 注册蓝图
def route(url, prefix='/'):
    name = url.split('.')[-1]
    if name == 'index':
        prefix = '/'
    else:
        prefix = '/'+name
    # print('路由：', name, prefix)
    return Blueprint(url.replace('.', '_'), prefix)

# 公式：（当前页-1）*每页条数，每页条数
def Page(page, limit=10):
    return (page-1)*limit

# 截取小数 不四舍五入
def cutNum(num, c=1):
    str_num = str(num)
    return float(str_num[:str_num.index('.') + 1 + c])
