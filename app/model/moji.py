from tortoise import Model, fields

# https://tortoise.github.io/
class Moji(Model):
    cityId = fields.IntField(pk=True)
    name = fields.CharField(50)

    # def __str__(self):
    #     return f"User {self.cityId}: {self.name}"