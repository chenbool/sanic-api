from util import json, db, route, Page

bp = route(__name__)

@bp.get("/")
async def index(request):
    return json({})

# 最新
@bp.get("/new/<page:int>")
async def new(request, page=1):
    params = {
        'page': page,
        'limit': 10
    }

    keyword = request.args.get('keyword')
    whereMap = "b.nickname like '%"+keyword+"%' or a.content like '%"+keyword+"%' "
    # 生成页数
    params['page'] = Page(params['page'], params['limit'])
    sql = 'SELECT a.*, b.nickname,b.account,b.avatar FROM tweet AS a INNER JOIN user AS b ON a.uid = b.id where '+whereMap+'  ORDER BY update_date desc LIMIT :page,:limit'
    res = db.query(sql, **params)
    return json(res.as_dict())

# 热门
@bp.get("/hot/<page:int>")
async def hot(request, page=1):
    params = {
        'page': page,
        'limit': 10
    }

    keyword = request.args.get('keyword')
    whereMap = "b.nickname like '%"+keyword+"%' or a.content like '%"+keyword+"%' "
    # 生成页数
    params['page'] = Page(params['page'], params['limit'])
    sql = 'SELECT a.*, b.nickname,b.account,b.avatar FROM tweet AS a INNER JOIN user AS b ON a.uid = b.id where '+whereMap+'  ORDER BY heart desc LIMIT :page,:limit'
    res = db.query(sql, **params)
    return json(res.as_dict())

# 视频
@bp.get("/video/<page:int>")
async def video(request, page=1):
    params = {
        'page': page,
        'limit': 10
    }

    keyword = request.args.get('keyword')
    whereMap = "b.nickname like '%"+keyword+"%' or a.content like '%"+keyword+"%' "
    # 生成页数
    params['page'] = Page(params['page'], params['limit'])
    sql = 'SELECT a.*, b.nickname,b.account,b.avatar FROM tweet AS a INNER JOIN user AS b ON a.uid = b.id where b.type="video" and '+whereMap+'  ORDER BY update_date desc LIMIT :page,:limit'
    res = db.query(sql, **params)
    return json(res.as_dict())

# 视频
@bp.get("/img/<page:int>")
async def img(request, page=1):
    params = {
        'page': page,
        'limit': 10
    }

    keyword = request.args.get('keyword')
    whereMap = "b.nickname like '%"+keyword+"%' or a.content like '%"+keyword+"%' "
    # 生成页数
    params['page'] = Page(params['page'], params['limit'])
    sql = 'SELECT a.*, b.nickname,b.account,b.avatar FROM tweet AS a INNER JOIN user AS b ON a.uid = b.id where b.type="img" and '+whereMap+'  ORDER BY update_date desc LIMIT :page,:limit'
    res = db.query(sql, **params)
    return json(res.as_dict())


# user
@bp.get("/user/<page:int>")
async def user(request, page=1):
    params = {
        'page': page,
        'limit': 10
    }

    keyword = request.args.get('keyword')
    whereMap = "nickname like '%"+keyword+"%' or desc like '%"+keyword+"%' "
    # 生成页数
    params['page'] = Page(params['page'], params['limit'])
    sql = 'SELECT * FROM user where '+whereMap+'  ORDER BY update_date desc LIMIT :page,:limit'
    res = db.query(sql, **params)
    return json(res.as_dict())