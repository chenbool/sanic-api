from util import json, db, cutNum, route, model

bp = route(__name__)

@bp.get("/")
async def index(request):
    rs = model('user').field('id, nickname').limit('1')
    return json(rs.as_dict())

@bp.get("/edit/<id:int>")
async def edit(request, id):
    return json({})


@bp.get("/city")
async def city(request):
    lng = float(request.args.get('lng'))
    lat = float(request.args.get('lat'))
    lng = cutNum(lng)
    lat = cutNum(lat)
    sql = 'select * from area where lng like "%'+str(lng)+'%" and lat like "%'+str(lat)+'%" AND level = 2 limit 0,1'
    res = db.query(sql)
    return json(res.as_dict())